import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.NoSuchElementException;


public class Subset {

    public static void main(String[] args) {
        int k = Integer.parseInt(args[0]);
        RandomizedQueue<String> rq = new RandomizedQueue<String>();
        while (true) {
            try {
                String s = StdIn.readString();
                rq.enqueue(s);
                if (rq.size() > k) {
                    rq.dequeue();
                }
            } catch (NoSuchElementException e) {
                break;
            }
        }
        for (String str : rq) {
            StdOut.println(str);
        }
    }

}