import edu.princeton.cs.algs4.*;

public class Solver {
    private MinPQ<SearchNode> origPQ;
    private MinPQ<SearchNode> twinPQ;
    private Queue<Board> backtrace;
    private boolean isSolvable;

    private class SearchNode implements Comparable<SearchNode> {

        private Board board;
        private int moves;
        private SearchNode parent;
        private int cachedKey;

        SearchNode(Board board, int moves, SearchNode parent) {
            this.board = board;
            this.moves = moves;
            this.parent = parent;
            this.cachedKey = -1;
        }

        SearchNode(Board board) {
            this(board, 0, null);
        }

        public int key() {
            if (cachedKey == -1) {
                cachedKey = moves + board.manhattan();
            }
            return cachedKey;
        }

        @Override
        public int compareTo(SearchNode that) {
            if (this.key() < that.key())
                return -1;
            if (this.key() > that.key())
                return +1;
            return 0;
        }
    }

    public Solver(Board initial) {
        isSolvable = true;

        SearchNode origSearchNode = new SearchNode(initial);
        SearchNode twinSearchNode = new SearchNode(initial.twin());

        origPQ = new MinPQ<SearchNode>();
        twinPQ = new MinPQ<SearchNode>();
        backtrace = new Queue<Board>();

        while (!(origSearchNode.board.isGoal() || twinSearchNode.board.isGoal()) && origSearchNode.moves < 111) {
            for (Board neighbor : origSearchNode.board.neighbors()) {
                if (origSearchNode.parent != null && origSearchNode.parent.board.equals(neighbor))
                    continue;
                origPQ.insert(new SearchNode(neighbor, origSearchNode.moves + 1,  origSearchNode));
            }
            origSearchNode = origPQ.delMin();

            for (Board neighbor : twinSearchNode.board.neighbors()) {
                if (twinSearchNode.parent != null && twinSearchNode.parent.board.equals(neighbor))
                    continue;
                twinPQ.insert(new SearchNode(neighbor, twinSearchNode.moves + 1, twinSearchNode));
            }
            twinSearchNode = twinPQ.delMin();
        }

        if (twinSearchNode.board.isGoal()) isSolvable = false;
        Stack<Board> boards = new Stack<Board>();
        while (origSearchNode.parent != null) {
            boards.push(origSearchNode.board);
            origSearchNode = origSearchNode.parent;
        }
        backtrace.enqueue(initial);
        while (!boards.isEmpty()) {
            backtrace.enqueue(boards.pop());
        }
    }

    public boolean isSolvable() {
        return isSolvable;
    }

    public int moves() {
        return isSolvable ? backtrace.size() - 1 : -1;
    }

    public Iterable<Board> solution() {
        return isSolvable ? backtrace : null;
    }

    public static void main(String[] args) {

        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);
        Solver solver = new Solver(initial);
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else
            StdOut.println("Minimum number of moves = " + solver.moves());
    }
}
