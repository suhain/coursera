import edu.princeton.cs.algs4.*;
import edu.rice.cs.javalanglevels.Pair;

import java.util.ArrayList;
import java.util.Comparator;


public class Sudoku {
    private class Grid {
        private int[][] grid;
        private int d;
        private int cachedBlankCellsCount;

        Grid(int dimension, int[][] grid) {
            d = dimension;
            this.grid = new int[d * d + 1][d * d + 1];

            for (int i = 1; i <= d * d; i++)
                this.grid[i][0] = this.grid[0][i] = 0;

            for (int i = 1; i <= d * d; i++)
                for (int j = 1; j <= d * d; j++)
                    this.grid[i][j] = grid[i - 1][j - 1];

            cachedBlankCellsCount = -1;
        }

        public int blankCellsCount() {
            if (cachedBlankCellsCount == -1) {
                cachedBlankCellsCount = 0;
                for (int i = 1; i <= d * d; i++) {
                    for (int j = 1; j <= d * d; j++) {
                        if (grid[i][j] == 0) {
                            cachedBlankCellsCount++;
                        }
                    }
                }
            }
            return cachedBlankCellsCount;
        }

        private boolean validateCol(int col) {
            int[] values = new int[d * d + 1];
            for (int i = 0; i <= d * d; i++) values[i] = 0;
            for (int i = 1; i <= d * d; i++) values[grid[i][col]]++;
            if (values[0] > 0) return false;
            for (int i = 1; i <= d * d; i++) if (values[i] != 1) return false;
            return true;
        }

        private boolean validateRow(int row) {
            int[] values = new int[d * d + 1];
            for (int i = 0; i <= d * d; i++) values[i] = 0;
            for (int i = 1; i <= d * d; i++) values[grid[row][i]]++;
            if (values[0] > 0) return false;
            for (int i = 1; i <= d * d; i++) if (values[i] != 1) return false;
            return true;
        }

        private boolean validateSquare(int row, int col) {
            int[] values = new int[d * d + 1];
            for (int i = 0; i <= d * d; i++) values[i] = 0;
            for (int i = 0; i < d; i++)
                for (int j = 0; j < d; j++)
                    values[grid[row + i][col + j]]++;
            if (values[0] > 0) return false;
            for (int i = 1; i <= d * d; i++) if (values[i] != 1) return false;
            return true;
        }

        public boolean isValid() {
            if (blankCellsCount() > 0) {
                return false;
            }

            for (int i = 1; i <= d * d; i++)
                if (!validateRow(i) || !validateCol(i))
                    return false;

            for (int i = 1; i <= d * d; i+=d)
                for (int j = 1; j <= d * d; j+=d)
                    if (!validateSquare(i, j)) return false;
            return true;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= d * d; i++) {
                for (int j = 1; j <= d * d; j++) {
                    sb.append(String.format("%d ", grid[i][j]));
                }
                sb.append("\n");
            }
            return sb.toString();
        }

        public Grid fill(int row, int col, int value) {
            int[][] grid = new int[d * d][d * d];
            for (int i = 0; i < d * d; i++)
                for (int j = 0; j < d * d; j++)
                    grid[i][j] = this.grid[i + 1][j + 1];
            grid[row][col] = value;
            return new Grid(d, grid);
        }

        public int validDistance() {
            int distance = 0;
            for (int i = 1; i <= d * d; i++) {
                if (validateRow(i)) distance++;
                if (validateCol(i)) distance++;
            }
            for (int i = 1; i <= d * d; i+=d)
                for (int j = 1; j <= d * d; j+=d)
                    if (validateSquare(i, j)) distance++;
            return distance;
        }

        public Pair<Integer, Integer> getBlankCell() {
            for (int i = 1; i <= d * d; i++) {
                for (int j = 1; j <= d * d; j++) {
                    if (grid[i][j] == 0) {
                        return new Pair<Integer, Integer>(i - 1, j - 1);
                    }
                }
            }
            throw new RuntimeException();
        }

        public ArrayList<Integer> getAvailableValues(int row, int col) {
            row = row + 1;
            col = col + 1;

            int[] hValues = new int[d * d + 1];
            for (int i = 1; i <= d * d; i++) hValues[i] = 0;
            for (int i = 1; i <= d * d; i++) hValues[grid[row][i]]++;

            int[] vValues = new int[d * d + 1];
            for (int i = 0; i <= d * d; i++) vValues[i] = 0;
            for (int i = 0; i <= d * d; i++) vValues[grid[i][col]]++;

            int[] sValues = new int[d * d + 1];
            for (int i = 0; i <= d * d; i++) sValues[i] = 0;
            int rx = 1, ry = 1;
            for (; !(rx <= row && row < rx + d); rx+=d);
            for (; !(ry <= col && col < ry + d); ry+=d);
            for (int i = rx; i < rx + d; i++)
                for (int j = ry; j < ry + d; j++) {
                    sValues[grid[i][j]]++;
                }

            ArrayList<Integer> result = new ArrayList<Integer>();
            for (int i = 1; i <= d * d; i++) {
                if (hValues[i] + vValues[i] + sValues[i] == 0)
                    result.add(i);
            }
            return result;
        }

    }

    private class ValidDistanceComparator implements Comparator<Grid> {
        public int compare(Grid g, Grid h) {
            if (g.validDistance() < h.validDistance())
                return -1;
            if (g.validDistance() > h.validDistance())
                return +1;
            return 0;
        }
    }

    private MaxPQ<Grid> pq;
    private ArrayList<Grid> solutions;

    Sudoku(int d, int[][] grid) {
        pq = new MaxPQ<Grid>(new ValidDistanceComparator());
        pq.insert(new Grid(d, grid));
        solutions = new ArrayList<Grid>();
        while (!pq.isEmpty()) {
            Grid g = pq.delMax();
            if (g.isValid()) {
                solutions.add(g);
            }
            if (g.blankCellsCount() > 0) {
                Pair<Integer, Integer> pair = g.getBlankCell();
                for (Integer i : g.getAvailableValues(pair.getFirst(), pair.getSecond())) {
                    pq.insert(g.fill(pair.getFirst(), pair.getSecond(), i));
                }
            }
        }
    }

    public Iterable<Grid> solution() {
        return solutions;
    }

    public static void main(String[] args) {
        In in = new In(args[0]);
        int dimension = in.readInt();
        int[][] grid = new int[dimension * dimension][dimension * dimension];
        for (int i = 0; i < dimension * dimension; i++)
            for (int j = 0; j < dimension * dimension; j++)
                grid[i][j] = in.readInt();
        Sudoku sudoku = new Sudoku(dimension, grid);

        for (Grid g : sudoku.solution())
            StdOut.println(g);
    }
}
