import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdRandom;

public class Percolation {
    private boolean[][] grid;
    private int n;
    private WeightedQuickUnionUF unionFind;
    private int top;
    private int bottom;

    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("got negative value");
        }
        grid = new boolean[n + 1][n + 1];
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                grid[i][j] = false;
            }
        }
        this.n = n;
        unionFind = new WeightedQuickUnionUF(n * n + 2);
        top = 0;
        bottom = n * n + 1;
    }

    public void open(int row, int col) {
        validate(row);
        validate(col);
        if (isOpen(row, col)) {
            return;
        }
        grid[row][col] = true;
        if (row - 1 > 0 && isOpen(row - 1, col)) {
            unionFind.union(
                    coordinatesToIdentifier(row, col),
                    coordinatesToIdentifier(row - 1, col)
            );
        }
        if (row + 1 <= n && isOpen(row + 1, col)) {
            unionFind.union(
                    coordinatesToIdentifier(row, col),
                    coordinatesToIdentifier(row + 1, col)
            );
        }
        if (col - 1 > 0 && isOpen(row, col - 1)) {
            unionFind.union(
                    coordinatesToIdentifier(row, col),
                    coordinatesToIdentifier(row, col - 1)
            );
        }
        if (col + 1 <= n && isOpen(row, col + 1)) {
            unionFind.union(
                    coordinatesToIdentifier(row, col),
                    coordinatesToIdentifier(row, col + 1)
            );
        }
        if (row == 1) {
            unionFind.union(top, coordinatesToIdentifier(row, col));
        }
        if (row == n) {
            unionFind.union(bottom, coordinatesToIdentifier(row, col));
        }
    }

    public boolean isOpen(int row, int col) {
        validate(row);
        validate(col);
        return grid[row][col];
    }

    public boolean isFull(int row, int col) {
        validate(row);
        validate(col);
        return unionFind.connected(top, coordinatesToIdentifier(row, col));
    }

    public boolean percolates() {
        return unionFind.connected(top, bottom);
    }

    private int coordinatesToIdentifier(int row, int col) {
        return (row - 1) * n + col;
    }

    private void validate(int index) {
        if (index < 1 || index > n) {
            throw new IndexOutOfBoundsException("index out of bound");
        }
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        Percolation percolation = new Percolation(n);
        int i = 0;
        while (!percolation.percolates()) {
            int row = StdRandom.uniform(1, n + 1);
            int col = StdRandom.uniform(1, n + 1);
            if (!percolation.isOpen(row, col)) {
                percolation.open(row, col);
                i++;
            }
        }
        System.out.println(n + " " + i);
    }

}