import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

public class KdTree {
    private int size = 0;

    private enum Orientation {
        Vertical, Horizontal;

        public Orientation switchOrientation() {
            return this.equals(Orientation.Horizontal) ? Orientation.Vertical : Orientation.Horizontal;
        }
    }

    private static class Node {
        private Point2D point;
        private RectHV rect;
        private Node l;
        private Node r;
        public Node(Point2D point) {
            this.point = point;
        }
    }

    private Node root;

    private Node put(Node x, Point2D p, Orientation orientation) {
        if (x == null) {
            size++;
            return new Node(p);
        }
        if (x.point.equals(p)) {
            return x;
        }
        int cmp = compare(p, x.point, orientation);
        Orientation nextOrientation = orientation.switchOrientation();
        if (cmp < 0) {
            x.l = put(x.l, p, nextOrientation);
            if (x.l.rect == null) {
                if (orientation == Orientation.Vertical) {
                    x.l.rect = new RectHV(x.rect.xmin(), x.rect.ymin(),
                            x.point.x(), x.rect.ymax());
                } else {
                    x.l.rect = new RectHV(x.rect.xmin(), x.rect.ymin(),
                            x.rect.xmax(), x.point.y());
                }
            }
        } else {
            x.r = put(x.r, p, nextOrientation);
            if (x.r.rect == null) {
                if (orientation == Orientation.Vertical) {
                    x.r.rect = new RectHV(x.point.x(), x.rect.ymin(),
                            x.rect.xmax(), x.rect.ymax());
                } else {
                    x.r.rect = new RectHV(x.rect.xmin(), x.point.y(),
                            x.rect.xmax(), x.rect.ymax());
                }
            }
        }
        return x;
    }

    private boolean contains(Node x, Point2D p, Orientation orientation) {
        if (x == null) {
            return false;
        }
        if (x.point.equals(p)) {
            return true;
        }
        int cmp = compare(p, x.point, orientation);
        Orientation nextOrientation = orientation.switchOrientation();
        if (cmp < 0) {
            return contains(x.l, p, nextOrientation);
        } else {
            return contains(x.r, p, nextOrientation);
        }
    }

    private int compare(Point2D p, Point2D q, Orientation orientation) {
        if (orientation == Orientation.Vertical) {
            return Double.compare(p.x(), q.x());
        } else {
            return Double.compare(p.y(), q.y());
        }
    }

    private void findPoints(Queue<Point2D> queue, RectHV rect, Node x) {
        if (!rect.intersects(x.rect)) {
            return;
        }
        if (rect.contains(x.point)) {
            queue.enqueue(x.point);
        }
        if (x.l != null) {
            findPoints(queue, rect, x.l);
        }
        if (x.r != null) {
            findPoints(queue, rect, x.r);
        }
    }

    private Point2D findNearest(Node x, Point2D p, Point2D nearest,
                                double nearestDistance, Orientation orientation) {
        if (x == null) {
            return nearest;
        }
        Point2D closest = nearest;
        double closestDistance = nearestDistance;
        double distance = x.point.distanceSquaredTo(p);
        if (distance < nearestDistance) {
            closest = x.point;
            closestDistance = distance;
        }
        Node first, second;
        if (orientation == Orientation.Vertical) {
            if (p.x() < x.point.x()) {
                first = x.l;
                second = x.r;
            } else {
                first = x.r;
                second = x.l;
            }
        } else {
            if (p.y() < x.point.y()) {
                first = x.l;
                second = x.r;
            } else {
                first = x.r;
                second = x.l;
            }
        }
        Orientation nextOrientation = orientation.switchOrientation();
        if (first != null && first.rect.distanceSquaredTo(p) < closestDistance) {
            closest = findNearest(first, p, closest, closestDistance,
                    nextOrientation);
            closestDistance = closest.distanceSquaredTo(p);
        }
        if (second != null
                && second.rect.distanceSquaredTo(p) < closestDistance) {
            closest = findNearest(second, p, closest, closestDistance,
                    nextOrientation);
        }

        return closest;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public int size() {
        return size;
    }

    public void insert(Point2D p) {
        if (isEmpty()) {
            root = new Node(p);
            root.rect = new RectHV(0, 0, 1, 1);
            size++;
            return;
        }
        root = put(root, p, Orientation.Vertical);
    }

    public boolean contains(Point2D p) {
        return contains(root, p, Orientation.Vertical);
    }

    public void draw() {
        draw(root, Orientation.Vertical);
    }

    private void draw(Node x, Orientation orientation) {
        if (x == null) {
            return;
        }
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        x.point.draw();
        if (orientation == Orientation.Vertical) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.setPenRadius(0.001);
            StdDraw.line(x.point.x(), x.rect.ymin(), x.point.x(), x.rect.ymax());
        } else {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.setPenRadius(0.001);
            StdDraw.line(x.rect.xmin(), x.point.y(), x.rect.xmax(), x.point.y());
        }
        Orientation next = orientation.switchOrientation();
        draw(x.l, next);
        draw(x.r, next);
    }

    public Iterable<Point2D> range(RectHV rect) {
        Queue<Point2D> queue = new Queue<Point2D>();

        if (!isEmpty()) {
            findPoints(queue, rect, root);
        }
        return queue;
    }

    public Point2D nearest(Point2D p) {
        if (isEmpty()) {
            return null;
        }
        return findNearest(root, p, root.point, Double.MAX_VALUE,
                Orientation.Vertical);
    }
}