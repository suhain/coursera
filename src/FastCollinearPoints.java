import edu.princeton.cs.algs4.*;

import java.util.ArrayList;
import java.util.Arrays;


public class FastCollinearPoints {
    private ArrayList<Point> segmentsP = new ArrayList<Point>();
    private ArrayList<Point> segmentsQ = new ArrayList<Point>();

    private void pushSegment(Point p, Point q) {
        // appends new segment to the answer
        for (int i = 0; i < segmentsP.size(); i++) {
            Point knownP = segmentsP.get(i);
            Point knownQ = segmentsQ.get(i);
            if (knownP.compareTo(p) == 0 && knownQ.compareTo(q) == 0) {
                return;
            }
            boolean sameSlope = knownP.slopeTo(knownQ) == p.slopeTo(q);
            boolean sameLine = (
                    knownP.slopeTo(p) == knownP.slopeTo(q) ||
                            knownQ.slopeTo(p) == knownQ.slopeTo(q)
            );
            if (sameSlope && sameLine) {
                if (knownP.compareTo(p) > 0) {
                    segmentsP.set(i, p);
                }
                if (knownQ.compareTo(q) < 0) {
                    segmentsQ.set(i, q);
                }
                return;
            }
        }
        segmentsP.add(p);
        segmentsQ.add(q);
    }

    private void validate(Point[] points) {
        // validates input data
        if (points == null) {
            throw new NullPointerException("");
        }
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null)
                throw new NullPointerException("");
        }
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                if (points[i].compareTo(points[j]) == 0) {
                    throw new IllegalArgumentException("");
                }
            }
        }
    }
    private Point min(Point p, Point q) {
        return p.compareTo(q) < 0 ? p : q;
    }
    private Point max(Point p, Point q) {
        return p.compareTo(q) > 0 ? p : q;
    }

    private void findCollinears(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            Point p = points[i];
            Point[] q = new Point[points.length - (i + 1)];
            int minus = 0;
            for (int j = i; j < points.length; j++) {
                if (i == j) {
                    minus = (i + 1);
                    continue;
                }
                q[j - minus] = points[j];
            }
            Arrays.sort(q, p.slopeOrder());
            int count = 0;
//            StdOut.println("For point " + p);
            for (int k = 1; k < q.length; k++) {
//                StdOut.println(q[k-1] + " with slope " + p.slopeTo(q[k-1]));
                if (p.slopeTo(q[k - 1]) == p.slopeTo(q[k])) {
                    count++;
                } else {
                    count = 0;
                }
                if (count == 2) {
                    Point w = q[k - 2];
                    Point r = q[k - 1];
                    Point s = q[k];
                    pushSegment(
                            min(min(p, w), min(r, s)),
                            max(max(p, w), max(r, s))
                    );
                }
                if (count > 2) {
                    pushSegment(
                            min(p, q[k]), max(p, q[k])
                    );
                }
            }
//            if (q.length > 1) StdOut.println(q[q.length-1] + " with slope " + p.slopeTo(q[q.length-1]));
//            StdOut.println("===");
        }
    }

    public FastCollinearPoints(Point[] points) {
        validate(points);
//        Arrays.sort(points);
        findCollinears(points);
    }
    public int numberOfSegments() {
        return segmentsQ.size();
    }
    public LineSegment[] segments() {
        LineSegment[] lineSegments = new LineSegment[segmentsP.size()];

        for (int i = 0; i != segmentsP.size(); i++) {
            lineSegments[i] = new LineSegment(segmentsP.get(i), segmentsQ.get(i));
        }
        return lineSegments;
    }

    public static void main(String[] args) {
        In in = new In(args[0]);      // input file
        Point[] points = new Point[in.readInt()];

        int x, y, i = 0;
        while (!in.isEmpty()) {
            x = in.readInt();
            y = in.readInt();
            points[i++] = new Point(x, y);
        }
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment ls : collinear.segments()) {
            System.out.println(ls);
        }
    }
}
