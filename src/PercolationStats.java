import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;


public class PercolationStats {

    private double[] results;

    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException("arguments should be positive");
        }
        results = new double[trials];
        for (int trial = 0; trial < trials; trial++) {
            Percolation percolation = new Percolation(n);
            int i = 0;
            while (!percolation.percolates()) {
                int row = StdRandom.uniform(1, n + 1);
                int col = StdRandom.uniform(1, n + 1);
                if (!percolation.isOpen(row, col)) {
                    i++;
                    percolation.open(row, col);
                }
            }
            results[trial] = 1.0 * i / n / n;
        }
    }

    public double mean() {
        return StdStats.mean(results);
    }

    public double stddev() {
        return StdStats.stddev(results);
    }

    public double confidenceLo() {
        return mean() - stddev() * 1.96 / Math.sqrt(results.length);
    }

    public double confidenceHi() {
        return mean() + stddev() * 1.96 / Math.sqrt(results.length);
    }

    public static void main(String[] args) {
        PercolationStats ps = new PercolationStats(
                Integer.parseInt(args[0]),
                Integer.parseInt(args[1])
        );
        System.out.println("mean       = " + ps.mean());
        System.out.println("stddev     = " + ps.stddev());
        System.out.println("confidence = " + ps.confidenceLo() + ", " + ps.confidenceHi());
    }

}
