import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by hus on 12/12/16.
 */
public class Deque<Item> implements Iterable<Item> {

    private class Node {
        Item value;
        Node next = null;
        Node prev = null;

        Node(Item value) {
            this.value = value;
        }
    }

    private Node head = null;
    private Node tail = null;
    private int size = 0;

    private void validateValue(Item value) {
        if (value == null) {
            throw new NullPointerException("null not allowed");
        }
    }

    public Deque() {
    }

    public boolean isEmpty() {
        return head == null && tail == null;
    }

    public void addFirst(Item value) {
        validateValue(value);
        Node node = new Node(value);
        if (isEmpty()) {
            head = tail = node;
        } else {
            node.next = head;
            head.prev = node;
            head = node;
        }
        size++;
    }

    public void addLast(Item value) {
        validateValue(value);
        Node node = new Node(value);
        if (isEmpty()) {
            head = tail = node;
        } else {
            node.prev = tail;
            tail.next = node;
            tail = node;
        }
        size++;
    }

    public Item removeFirst() {
        if (isEmpty()) {
            throw new NoSuchElementException("Deque is empty");
        }
        Item value = head.value;
        head = head.next;
        if (head != null) {
            head.prev = null;
        } else {
            tail = null;
        }
        size--;
        return value;
    }

    public Item removeLast() {
        if (isEmpty()) {
            throw new NoSuchElementException("Deque is empty");
        }
        Item value = tail.value;
        tail = tail.prev;
        if (tail != null) {
            tail.next = null;
        } else {
            head = null;
        }
        size--;
        return value;
    }

    public int size() {
        return size;
    }

    @Override
    public Iterator<Item> iterator() {
        return new DequeueIterator();
    }

    private class DequeueIterator implements Iterator<Item> {
        private Node current = head;

        public boolean hasNext() {
            return current != null;
        }

        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException("No elements left to return");
            }
            Item value = current.value;
            current = current.next;
            return value;
        }
    }

    public static void main(String[] args) {
    }
}
