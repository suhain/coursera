import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by hus on 1/23/17.
 */
public class BruteCollinearPoints {

    private ArrayList<Point> segmentsP = new ArrayList<Point>();
    private ArrayList<Point> segmentsQ = new ArrayList<Point>();

    private void pushSegment(Point p, Point q) {
        // appends new segment to the answer
        for (int i = 0; i < segmentsP.size(); i++) {
            Point knownP = segmentsP.get(i);
            Point knownQ = segmentsQ.get(i);
            if (knownP.compareTo(p) == 0 && knownQ.compareTo(q) == 0) {
                return;
            }
            boolean sameSlope = knownP.slopeTo(knownQ) == p.slopeTo(q);
            boolean sameLine = (
                    knownP.slopeTo(p) == knownP.slopeTo(q) ||
                            knownQ.slopeTo(p) == knownQ.slopeTo(q)
            );
            if (sameSlope && sameLine) {
                if (knownP.compareTo(p) > 0) {
                    segmentsP.set(i, p);
                }
                if (knownQ.compareTo(q) < 0) {
                    segmentsQ.set(i, q);
                }
                return;
            }
        }
        segmentsP.add(p);
        segmentsQ.add(q);
    }

    private void validate(Point[] points) {
        if (points == null) {
            throw new NullPointerException("");
        }
        for (int i = 0; i < points.length; i++) {
            if (points[i] == null)
                throw new NullPointerException("");
        }
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                if (points[i].compareTo(points[j]) == 0) {
                    throw new IllegalArgumentException("");
                }
            }
        }
    }

    private Point min(Point p, Point q) {
        return p.compareTo(q) < 0 ? p : q;
    }
    private Point max(Point p, Point q) {
        return p.compareTo(q) > 0 ? p : q;
    }

    public BruteCollinearPoints(Point[] points) {
        validate(points);
        for (int i1 = 0; i1 < points.length; i1++) {
            Point p = points[i1];
            for (int i2 = i1 + 1; i2 < points.length; i2++) {
                Point q = points[i2];
                double slopeQ = p.slopeTo(q);
                for (int i3 = i2 + 1; i3 < points.length; i3++) {
                    Point r = points[i3];
                    double slopeR = p.slopeTo(r);
                    if (slopeQ == slopeR) {
                        for (int i4 = i3 + 1; i4 < points.length; i4++) {
                            Point s = points[i4];
                            double slopeS = p.slopeTo(s);
                            if (slopeR == slopeS) {
                                pushSegment(
                                        min(min(p, q), min(r, s)),
                                        max(max(p, q), max(r, s))
                                );
                            }
                        }
                    }
                }
            }
        }

    }

    public LineSegment[] segments() {
        LineSegment[] lineSegments = new LineSegment[segmentsP.size()];

        for (int i = 0; i != segmentsP.size(); i++) {
            lineSegments[i] = new LineSegment(segmentsP.get(i), segmentsQ.get(i));
        }
        return lineSegments;
    }

    public int numberOfSegments() {
        return segmentsP.size();
    }

    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
        }
    }

}
