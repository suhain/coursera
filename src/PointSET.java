import edu.princeton.cs.algs4.*;

public class PointSET {
    private final SET<Point2D> s = new SET<Point2D>();

    public boolean isEmpty() {
        return s.isEmpty();
    }

    public int size() {
        return s.size();
    }

    public void insert(Point2D p) {
        if (s.contains(p)) {
            return;
        }
        s.add(p);
    }

    public boolean contains(Point2D p) {
        return s.contains(p);
    }

    public void draw() {
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(.01);
        for (Point2D p : s) {
            p.draw();
        }
    }

    public Iterable<Point2D> range(RectHV rect) {
        Queue<Point2D> queue = new Queue<Point2D>();
        for (Point2D p : s) {
            if (rect.contains(p)) {
                queue.enqueue(p);
            }
        }
        return queue;
    }

    public Point2D nearest(Point2D p) {
        double distance = Double.MAX_VALUE;
        Point2D nearest = null;
        for (Point2D other : s) {
            if (p.distanceTo(other) < distance) {
                distance = p.distanceTo(other);
                nearest = other;
            }

        }
        return nearest;
    }
}