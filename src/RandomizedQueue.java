import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] elements;
    private int currentSize;

    private void resize(int newCapacity) {
        Object[] newElements = new Object[newCapacity];
        for (int i = 0; i < currentSize; i++) {
            newElements[i] = elements[i];
        }
        elements = (Item[]) newElements;
    }

    private void ensureCapacity() {
        if (elements.length == currentSize) {
            resize(elements.length * 2);
        }

        if (elements.length == currentSize * 4) {
            resize(elements.length / 2);
        }
    }

    private int capacity() {
        return elements.length;
    }

    public RandomizedQueue() {
        elements = (Item[]) new Object[1];
        currentSize = 0;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public int size() {
        return currentSize;
    }

    public void enqueue(Item value) {
        if (value == null) throw new NullPointerException("null not allowed");
        ensureCapacity();
        elements[currentSize++] = value;
    }

    public Item dequeue() {
        if (currentSize == 0) throw new NoSuchElementException("queue is empty");
        ensureCapacity();
        int randomIndex = StdRandom.uniform(currentSize);
        Item value = elements[randomIndex];
        elements[randomIndex] = elements[--currentSize];
        return value;
    }

    public Item sample() {
        if (currentSize == 0) throw new NoSuchElementException("queue is empty");
        int randomIndex = StdRandom.uniform(currentSize);
        return elements[randomIndex];
    }

    private class RandomizedQueueIterator<T> implements Iterator<T> {

        private T[] elements;
        private int[] order;
        private int currentIndex;

        public RandomizedQueueIterator(RandomizedQueue<T> rq) {
            order = new int[rq.size()];
            for (int i = 0; i < order.length; i++) order[i] = i;
            for (int i = 0; i < order.length; i++) {
                int ri = StdRandom.uniform(order.length);
                int rj = StdRandom.uniform(order.length);
                int tmp = order[ri];
                order[ri] = order[rj];
                order[rj] = tmp;
            }
            currentIndex = 0;
            elements = rq.elements;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < order.length;
        }

        @Override
        public T next() {
            if (!hasNext()) throw new NoSuchElementException("");
            return elements[order[currentIndex++]];
        }
    }

    @Override
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator<Item>(this);
    }

    public static void main(String[] args) {
    }

}